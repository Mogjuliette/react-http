import axios from "axios";
import { DogI } from "./entities";

/* export async function fetchAllDogs() {
    const response = await fetch('http://localhost:8000/api/dog');
    const data = await response.json();
    return data;
}
 */
/* avec axios : */

export async function fetchAllDogs() {
    const response = await axios.get<DogI[]>('http://localhost:8000/api/dog');
    return response.data;
}

export async function fetchOneDog(id:number) {
    const response = await axios.get<DogI>('http://localhost:8000/api/dog/'+id);
    return response.data;
}

export async function postDog(dog:DogI){
    const response = await axios.post<DogI>('http://localhost:8000/api/dog', dog);
    return response.data;
} 