export interface DogI {
    id?:number;
    name:string;
    breed?:string;
    birthdate?:string;
}
