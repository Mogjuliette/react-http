import AllDogs from "@/components/AllDogs";
import FormDog from "@/components/FormDog";

export default function Add(){

    return(
        <main>
            <h1>Ajouter un chien</h1>
            <FormDog />
        </main>
    )
}