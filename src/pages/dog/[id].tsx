import Dog from "@/components/Dog";
import { fetchOneDog } from "@/dog-service";
import { DogI } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
   

export default function DogPage(){
    const router = useRouter();
    const {id} = router.query;
    const [dog, setDog] = useState<DogI>();

   useEffect(() => {
        (async() =>{
            if(!id){
                return;
            }
            fetchOneDog(Number(id))
            .then(data => {setDog(data)}) 
            .catch(error => {
                if(error.response.status == 404){
                    router.push('/404')
                }
            })
        })();
    }, [id]);

    if(!dog){
        return(<p>Loading</p>)
    }

    return (
        <>
            <h1>Dog {id}</h1>
            <h2>{dog?.name}</h2>
            <p>{dog?.breed}</p>
        </>
    );
}