import {DogI} from "@/entities";
import Link from "next/link";

interface Props {
    dog:DogI;
}

export default function Dog({dog}:Props){
        
    return (
        <div>
            <h3>{dog.name}</h3>
            <p>{dog.breed}</p>
            {dog.birthdate && <p>né.e le {new Date(dog.birthdate).toLocaleDateString("fr")}</p>}
            <Link href={"/dog/"+dog.id}>Voir plus</Link>
        </div>)
}