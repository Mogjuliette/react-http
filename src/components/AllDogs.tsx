import { fetchAllDogs } from "@/dog-service";
import { DogI } from "@/entities";
import { useEffect, useState } from "react";
import Dog from "@/components/Dog";



export default function AllDogs() {

    const [dogs, setDogs] = useState<DogI[]>([]);

    useEffect(() => {
        fetchAllDogs().then(data => {
            setDogs(data)
        }) 
            
    }, [])

    return (
        <>
            <h1>List of dogs</h1>
            <>{dogs.map((item) => <Dog key={item.id} dog={item}/>)}</>
        </>
    );
}