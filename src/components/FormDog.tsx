import { postDog } from "@/dog-service";
import { DogI } from "@/entities";
import { FormEvent, useState } from "react";


export default function FormDog(){

    const [form, setForm] = useState<DogI>({
        name: '',
        breed: '',
        birthdate: ''
    });
    
    function handleChange(event:any) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        postDog(form);
        setForm({
            name: '',
            breed: '',
            birthdate: ''
        });    
    }

    return (
        <>
            <form onSubmit={handleSubmit}>
                <label htmlFor="name">Name : </label>
                <input type="text" name="name" value={form.name} onChange={handleChange} />

                <label htmlFor="breed">Breed : </label>
                <input type="text" name="breed" value={form.breed} onChange={handleChange}  />

                <label htmlFor="birthdate">Birthdate : </label>
                <input type="date" name="birthdate" value={form.birthdate} onChange={handleChange}  />
                <button>Add Dog</button>
            </form>
            
        </>
    );


}